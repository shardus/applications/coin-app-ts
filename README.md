
## Prerequsites
Check shardus documentation's installation section to setup prerequsites.

## Install global packages

```
npm i -g shardus
npm i -g node-gyp 
```

## Install project dependencies
```
npm install
```
## Iterate on a single node

1. Make code changes to `index.ts` and / or `client.js`

2. Start the `seed-node-server`, `monitor-server`, and your `index.js` server:

    ```
    $ npm start
    ```

3. Interact with your `index.js` server:

    ```
    $ npm run client
    $ client$ help
    ...
    ```

4. Stop the `seed-node-server` and `monitor-server`, and clean residual run files:

    ```
    $ npm stop && npm run clean
    ```

Repeat until desired behavior is achieved...

## Using shardus cli to start the network
This method allows use to start a network consisting as many node as we want.
Since shardus cli read `package.json` to find `index.js` to start the nodes and our codes is in typescript, let's compile our code

To compile 
```bash
npm run compile
```
To start the network consisting 20 nodes.
```bash
shardus create-net 20
```

To interact with the network
```bash
npm run client
```
To stop the network -
```bash
shardus stop-net 
```

To clean residual folders and files, please do - 
```bash
shardus clean-net
```
